import numpy as np
from tango import DebugIt, DevState
from tango.server import Device, attribute, command, device_property

WAVEFORM_NUM_SAMPLES = 128
WAVEFORM_AMPLITUDE = 100
WAVEFORM_LENGTH_SEC = 0.007
MAX_COMMAND_QUEUE_LENGTH = 100


class Exciter(Device):
    host = device_property(dtype=str)
    port = device_property(dtype=int, default_value=3444)
    subarray_id = device_property(dtype=int, default_value=0)
    site = device_property(dtype=str, default_value="Skibotn")
    serial_num = device_property(dtype=str)

    @DebugIt()
    def init_device(self):
        super().init_device()
        self.set_state(DevState.INIT)
        self.set_status("Initialising")
        self._name = f"{self.site}:{self.subarray_id}-{self.serial_num}"
        self._timebase = np.linspace(0, WAVEFORM_LENGTH_SEC, WAVEFORM_NUM_SAMPLES)
        angles = np.linspace(0, 8 * np.pi, WAVEFORM_NUM_SAMPLES)
        self._pol_a_waveform = np.array(np.sin(angles) * WAVEFORM_AMPLITUDE, dtype=int)
        self._pol_b_waveform = np.array(np.cos(angles) * WAVEFORM_AMPLITUDE, dtype=int)
        self._command_queue = ["dummy1", "dummy2"]
        self.set_state(DevState.RUNNING)
        self.set_status("Ready")

    @attribute(dtype=str)
    @DebugIt()
    def name(self):
        return self._name

    @attribute(dtype=(float,), max_dim_x=WAVEFORM_NUM_SAMPLES, unit="sec")
    def waveform_timebase(self):
        """Waveform timebase."""
        return self._timebase

    @attribute(dtype=(int,), max_dim_x=WAVEFORM_NUM_SAMPLES)
    def pol_a_waveform(self):
        """Polarisation A waveform."""
        return self._pol_a_waveform

    @pol_a_waveform.write
    def pol_a_waveform(self, value):
        self._pol_a_waveform = value

    @attribute(dtype=(int,), max_dim_x=WAVEFORM_NUM_SAMPLES)
    def pol_b_waveform(self):
        """Polarisation B waveform."""
        return self._pol_b_waveform

    @pol_b_waveform.write
    def pol_b_waveform(self, value):
        self._pol_b_waveform = value

    @attribute(dtype=(str,), max_dim_x=MAX_COMMAND_QUEUE_LENGTH)
    def command_queue(self):
        return self._command_queue

    @command()
    @DebugIt()
    def clear_command_queue(self):
        self._command_queue = []


if __name__ == "__main__":
    Exciter.run_server()