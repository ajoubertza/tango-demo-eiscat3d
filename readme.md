# Example Tango deployment for EISCAT3D knowledge sharing

This is a very simple demo of a Tango Control system.
It has a few trivial Tango devices and the Taranta Suite with
a simple web UI.  It was created for the [EISCAT3D project](https://eiscat.se/eiscat3d-information/)
on behalf of [MAX IV](https://www.maxiv.lu.se).

### Pre-requisites

- Git
- Make
- Docker (with Docker Compose)

### Installation

Clone some repos:
```shell
git clone https://gitlab.com/tango-controls/web/taranta-suite.git
cd taranta-suite
git checkout f64cc5ed
git clone https://gitlab.com/ajoubertza/tango-demo-eiscat3d.git
```

Replace the `taranta-suite/docker-compose.yml` and `taranta-suite/Makefile` files with the files from this repo:
```shell
cp tango-demo-eiscat3d/docker-compose.yml .
cp tango-demo-eiscat3d/Makefile .
```

Clone some more repos, build local Docker images, and start Docker Compose services (this may take 10 minutes):
```shell
make run
```

Ctrl+C if you want to shut down the Docker Compose services.

### Usage

#### Start-up

If the Docker Compose services are not already running, start them from the `taranta-suite` directory:
```shell
docker compose up
```

Start the Tango device servers.  Open a new terminal window, and start a bash shell inside the `dev` container:
```shell
docker compose exec -ti dev bash
```

Register all the Tango device servers and devices, and set some device properties
inside the `dev` container:
```shell
tango_admin --add-server Exciter/s1 Exciter no-sk/exc/001,no-sk/exc/002,no-sk/exc/003,no-sk/exc/004
tango_admin --add-property no-sk/exc/001 host no-sk-ex001
tango_admin --add-property no-sk/exc/002 host no-sk-ex002
tango_admin --add-property no-sk/exc/003 host no-sk-ex003
tango_admin --add-property no-sk/exc/004 host no-sk-ex003
tango_admin --add-property no-sk/exc/001 serial_num EX12345-v1
tango_admin --add-property no-sk/exc/002 serial_num EX12346-v1
tango_admin --add-property no-sk/exc/003 serial_num EX12347-v1
tango_admin --add-property no-sk/exc/004 serial_num EX12348-v1

tango_admin --add-server Transmitter/s1 Transmitter no-sk/tx/001,no-sk/tx/002,no-sk/tx/003,no-sk/tx/004
tango_admin --add-property no-sk/tx/001 host no-sk-tx001
tango_admin --add-property no-sk/tx/002 host no-sk-tx002
tango_admin --add-property no-sk/tx/003 host no-sk-tx003
tango_admin --add-property no-sk/tx/004 host no-sk-tx003
tango_admin --add-property no-sk/tx/001 serial_num TX12345-v1
tango_admin --add-property no-sk/tx/002 serial_num TX12346-v1
tango_admin --add-property no-sk/tx/003 serial_num TX12347-v1
tango_admin --add-property no-sk/tx/004 serial_num TX12348-v1

tango_admin --add-server Receiver/s1 Receiver no-sk/rx/001
tango_admin --add-property no-sk/rx/001 host no-sk-rx001
tango_admin --add-property no-sk/rx/001 serial_num RX12345-v1
```

Run all the Tango device servers inside the `dev` container (you should be in the `/tango` directory):
```shell
python Exciter.py s1 -v4 &
python Transmitter.py s1 -v4 &
python Receiver.py s1 -v4 &
```
Leave out the `-v4` if you don't want to see the debug logs.

Use `kill` if you want to stop any of the Tango device server processes.

#### Taranta

Open `http://localhost:22484/testdb` in your browser.  On the Devices tab you can browse devices.

If you want to make any changes you need to log in.  See the `taranta-suite/users.json` file for account details.

On the Dashboards tab, select _Dashboards_ on the right and click _Import dashboard_.
Load the file EISCAT3D.wj.  Click the _Start_ button near the top left to launch it.

Example view:

![image](./dashboard.png)

### Useful links

- Taranta documentation https://taranta.readthedocs.io
- PyTango documentation: https://pytango.readthedocs.io
- More simple PyTango examples: https://gitlab.com/tango-controls/pytango/-/tree/develop/examples/training
- dsconfig tool for configuring Tango database via CLI: https://gitlab.com/MaxIV/lib-maxiv-dsconfig
- Jupyter plug-in for viewing Tango attributes live in Jupyter session: https://gitlab.com/tango-controls/jupyTango
- Tango on Slack: https://tango-controls.slack.com/
- Some Astron LOFAR 2.0 Tango code: https://git.astron.nl/lofar2.0/tango
- SKA Telescope developer portal: https://developer.skao.int/en/latest/