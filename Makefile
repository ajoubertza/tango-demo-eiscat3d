.PHONY: all run

all : taranta tangogql taranta-auth taranta-dashboard

taranta :
	git clone --branch 1.3.9 https://gitlab.com/tango-controls/web/taranta.git

tangogql :
	git clone https://gitlab.com/tango-controls/web/tangogql.git
	git -C tangogql checkout 7ce52b802eb02104195524694756f414dea7f801

taranta-auth :
	git clone https://gitlab.com/tango-controls/web/taranta-auth.git
	git -C taranta-auth checkout f0a2d2081692f5a5d0eead5cf3a669429be9dd12

taranta-dashboard :
	git clone --branch 1.2.0 https://gitlab.com/tango-controls/web/taranta-dashboard.git
	git -C taranta-dashboard checkout 78a58b2f3b3d281c275b0b9459c1573dd337c30b

run : all
	docker compose build && docker compose up
