from tango import DebugIt, DevState
from tango.server import Device, attribute, command, device_property

MAX_COMMAND_QUEUE_LENGTH = 100


class Transmitter(Device):
    host = device_property(dtype=str)
    port = device_property(dtype=int, default_value=3445)
    subarray_id = device_property(dtype=int, default_value=0)
    site = device_property(dtype=str, default_value="Skibotn")
    serial_num = device_property(dtype=str)

    @DebugIt()
    def init_device(self):
        super().init_device()
        self.set_state(DevState.INIT)
        self.set_status("Initialising")
        self._name = f"{self.site}:{self.subarray_id}-{self.serial_num}"
        self._output_power_w = 500.0
        self._command_queue = ["dummy1", "dummy2"]
        self.set_state(DevState.RUNNING)
        self.set_status("Ready")

    @attribute(dtype=str)
    @DebugIt()
    def name(self):
        return self._name

    @attribute(dtype=float, unit="W")
    def power(self):
        """Transmitter output power."""
        return self._output_power_w

    @power.write
    def power(self, value):
        self._output_power_w = value

    @attribute(dtype=(str,), max_dim_x=MAX_COMMAND_QUEUE_LENGTH)
    def command_queue(self):
        return self._command_queue

    @command()
    @DebugIt()
    def clear_command_queue(self):
        self._command_queue = []


if __name__ == "__main__":
    Transmitter.run_server()